-- -*- indent-tabs-mode: nil -*- --

module Control.Distributed.CCTools.WorkQueue.Task
    (
     
     -- * Task specification
      task, specifyFile, specifyBuffer, specifyFileCommand, specifyTag, specifyAlgorithm, delete

    -- * Properties
    , tag, command, workerSelectionAlgorithm, output, taskID, returnStatus, result, host
    , submitTime, finishTime, appDelayTime, timeSendInputStart, timeSendInputFinish
    , sendTime, timeExecuteCmdStart, timeExecuteCmdFinish, timeExecuteCmd
    , timeReceiveOutputStart, timeReceiveOutputFinish, timeReceiveOutput
    , totalBytesTransferred, totalTransferTime, totalCmdExecutionTime

    )
    where


import Control.Distributed.CCTools.WorkQueue.Internal.Task
