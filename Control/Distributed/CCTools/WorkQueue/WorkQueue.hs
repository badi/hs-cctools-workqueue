-- -*- indent-tabs-mode: nil -*- --

module Control.Distributed.CCTools.WorkQueue.WorkQueue
    (

    -- * WorkQueue specification
      defaultQParams
    , workqueue, setName, setFastAbort, setTaskOrdering, setPriority, setMasterMode, setScheduleAlg, shutdownWorkers

    -- * Logging
    , setLog

    -- * Task Submission and Retrieval
    , submit, eventLoop
    , wait, cancelTaskID, cancelTaskTag

    -- * Properties
    , getHunger, isFull, isEmpty, getPort, getName

    -- * Runtime Statistics
    , getWorkerSummary
    , getStats

    -- ** Workers
    , workersInit, workersReady, workersBusy, workersCancelling
    , totalWorkersJoined, totalWorkersRemoved

    -- ** Tasks
    , tasksRunning, tasksWaiting, tasksComplete
    , totalTasksDispatched, totalTasksComplete

    -- ** Data
    , totalBytesSent, totalBytesReceived

    -- ** Times
    , startTime, totalSendTime, totalReceiveTime

    )
    where


import  Control.Distributed.CCTools.WorkQueue.Internal.WorkQueue
