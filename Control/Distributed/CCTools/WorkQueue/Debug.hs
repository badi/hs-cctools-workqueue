-- -*- indent-tabs-mode: nil -*- --

module Control.Distributed.CCTools.WorkQueue.Debug
    (
      DebugFlag(..)
    , setDebugFlags
    )
    where

import Control.Distributed.CCTools.WorkQueue.Internal.Debug
import Control.Distributed.CCTools.WorkQueue.Internal.Types
