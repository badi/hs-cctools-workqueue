-- -*- indent-tabs-mode: nil -*- --

module Control.Distributed.CCTools.WorkQueue.Types
    (

     -- * Utility Types

     -- ** Positive numbers
       NPlus, nplus

     -- ** Unit-typed values

     -- | These values are instances of 'Marshallable'. You can use
     -- the 'marshal' function to extract the actual value

     -- *** Data
    , Bytes, DataSize
    , bytes

     -- *** Time
    , Seconds, MicroSeconds, EpochTime, DiffTime
    , epochSeconds, epochMicroSeconds
    , diffTime


     -- *** File Locality
    , Local, Remote, Location
    , local, remote


    -- * WorkQueue API

    -- ** WorkQueue, Task, and runtime stats
    , WorkQueue, Task, Stats

    -- ** WorkQueue Parameters
    , QueueParams(..)

    -- ** WorkQueue properties
    , MasterMode(..)
    , Hunger(..)
    , FastAbort(..)
    , Timeout(..), seconds
    , Cached
    , WorkerScheduleAlg(..)
    , TaskOrdering(..)
    , FileType(..)
    , Port, port

    -- ** Task properties
    , Command, cmd
    , TaskID
    , Hostname, hostname

    -- ** Debugging
    , DebugFlag(..)

    -- ** Conversions and marshalling
    , Castable(..)

    )
    where


import Control.Distributed.CCTools.WorkQueue.Internal.Types
import Control.Distributed.CCTools.WorkQueue.Internal.Cast
import Control.Distributed.CCTools.WorkQueue.Internal.CastInstances ()
