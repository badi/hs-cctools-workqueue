-- -*- indent-tabs-mode: nil -*- --

{-# LANGUAGE MultiParamTypeClasses #-}

module Control.Distributed.CCTools.WorkQueue.Internal.Cast where

class Castable a b where
    cast :: a -> b
