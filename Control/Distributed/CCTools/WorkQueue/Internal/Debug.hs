-- -*- indent-tabs-mode: nil -*- --

module Control.Distributed.CCTools.WorkQueue.Internal.Debug where

import Control.Distributed.CCTools.WorkQueue.Internal.Types (DebugFlag (..))

import Bindings.CCTools.Debug

import Foreign.C.String (CString, newCString)


setDebugFlags :: [DebugFlag] -> IO ()
setDebugFlags = mapM_ ( (c'cctools_debug_flags_set =<<) . cDebugFlag )
    where
      cDebugFlag          :: DebugFlag -> IO CString
      cDebugFlag All       = newCString "all"
      cDebugFlag WorkQueue = newCString "wq"

